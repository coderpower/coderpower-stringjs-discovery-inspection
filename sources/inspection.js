var S = require('string');

module.exports = function inspection(foo) {
    foo.isEmpty = S('').isEmpty();        // true
    foo.isEmpty = S('\n').isEmpty();      // true
    foo.isEmpty = S(null).isEmpty();      // true
    foo.isEmpty = S(undefined).isEmpty(); // true
    console.log('Is empty', foo.isEmpty);

    foo.isLower = S('well').isLower(); // true
    console.log('Is lower', foo.isLower);

    foo.isLower = S('Well Done').isLower(); // false
    console.log('Is lower', foo.isLower);

    foo.isNumeric = S('12').isNumeric(); // true
    console.log('Is numeric', foo.isNumeric);

    foo.isAlpha = S('12').isAlpha(); // false
    console.log('Is alpha', foo.isAlpha);

    return foo;
};

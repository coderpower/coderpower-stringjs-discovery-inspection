var S = require('string');
var chai = require('chai');
var expect = chai.expect;

// Sources
var inspection = require('../sources/inspection');
var foo = inspection({});

describe('String inspection', function() {
    it('should return an object', function(done) {
        expect(foo).to.be.an('object');
        done();
    });
    it('the object should have the keys: isEmpty, isLower, isNumeric, isAlpha', function(done) {
        expect(foo.isEmpty).to.exist;
        expect(foo.isLower).to.exist;
        expect(foo.isNumeric).to.exist;
        expect(foo.isAlpha).to.exist;
        done();
    });
    it('The keys: isEmpty and isNumeric should be true', function(done) {
        expect(foo.isEmpty).to.be.true;
        expect(foo.isNumeric).to.be.true;
        done()
    });
    it('The keys: isLower and isAlpha should be false', function(done) {
        expect(foo.isLower).to.be.false;
        expect(foo.isAlpha).to.be.false;
        done();
    });
});

### String inspection

In this example we will cover the following functions:

```javascript
    S('').isEmpty()
    S('hello').isLower()
    S('123').isNumeric()
    S('Hello world').isAlpha()
```

For more information, please refer to the documentation: http://stringjs.com.

